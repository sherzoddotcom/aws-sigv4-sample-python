from chalice import Chalice

app = Chalice(app_name='hoot')

@app.route('/')
def index():
    return {'hello': 'world'}

@app.route('/greet', methods=["POST"])
def greet():
    try:
        request_data = app.current_request.json_body
        if request_data and 'name' in request_data:
            return {'hello': request_data['name']}
    except Exception as e:
        print(e)
    return {'hello': 'stranger'}
