# Python resources for testing AWS SigV4

This repo contains two bits needed to test AWS Sig V4 (part of the [blog](#blog-link))
- Private API Gateway and Lambda, created by AWS Chalice
- Python scripts to make HTTP requests to API Gateway endpoints

## Private API Gateway/Lambda

This section needs few things to exist:
- VPC endpoint to API Gateway 
- Private subnets hosting that endpoint, which will also host the lambda ENI
- Security group in the same VPC that will be attached to lambda ENI

This [GitLab repo](https://gitlab.com/sherzoddotcom/aws-sigv4-sample-terraform) has a Terraform config that can set that up.

Assuming all these three things exist, open `hoot/.chalice/config.json` and set the values for `subnet_ids`, `security_group_ids` and `api_gateway_endpoint_vpce`. From there, we deploy the AWS resources (be sure to have valid AWS credentials and/or set the AWS_PROFILE env variable accordingly)

```console
$ python3 -m venv venv
$ . venv/bin/activate
(venv) $ pip install -r requirements.txt
(venv) $ cd hoot
(venv) $ chalice deploy
```

The last command should create the AWS resources within a minute, and output the API endpoint. We need that endpoint to use with the python scripts that will run the tests.

## Python scripts 

The scripts under `test-scripts` folder were copied from https://docs.aws.amazon.com/general/latest/gr/sigv4-signed-request-examples.html and modified for API Gateway testing. The `post.py` one uses temporary credentials to sign the HTTP request - suitable for cases where using long term credentials is not an option.

For this test, the scripts need to be present in an EC2 instance created in the same subnet as the VPC endpoint to reach it. The Terraform config mentioned in the above section does create such instance which can be accessed with SSM Session Manager - as long as there is a way to copy the scripts into that EC2 instance (I simply copy and pasted into SSM led session). Those scripts could be delivered with the same Terraform config, but that's an exercise for the reader.